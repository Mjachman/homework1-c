﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Class1
    {
        static double ConvertToMeters(int Unit, double valueLen)
        {
            double meters = valueLen;
            return meters;
        }


        static double ConvertToMeters(string ConvertFrom, double length)
        {
            double ConvertionRatio;


            switch (ConvertFrom)
            {
                case "1":
                    ConvertionRatio = 1;
                    break;
                case "2":
                    ConvertionRatio = 0.01;
                    break;
                case "3":
                    ConvertionRatio = 1000;
                    break;
                case "4":
                    ConvertionRatio = 0.9144;
                    break;
                case "5":
                    ConvertionRatio = 1852;
                    break;
                case "6":
                    ConvertionRatio = 1609;
                    break;

                default:
                    Console.Error.WriteLine("Nieznana jednostka wejściowa");
                    ConvertionRatio = 0;
                    // meters = 0;
                    break;
                    //return metres = 0;
            } //switch end  

            double meters = length * ConvertionRatio;
            //Console.WriteLine("Converted from:" + ConvertFrom);
            return meters;

        }//  ConvertToMeters function End 

        static double ConvertToOutputUnit(string ConvertTo, double LengthInMetres)
        {
            double ConvertionRatio;
            switch (ConvertTo)
            {
                case "1":
                    ConvertionRatio = 1;
                    break;
                case "2":
                    ConvertionRatio = 1 / 0.01;
                    break;
                case "3":
                    ConvertionRatio = 0.001;
                    break;
                case "4":
                    ConvertionRatio = 1 / 0.9144;
                    break;
                case "5":
                    ConvertionRatio = 0.0005399568;
                    break;
                case "6":
                    ConvertionRatio = 0.00062150403;
                    break;
                default:
                    Console.Error.WriteLine("Nieznana jednostka wyjściowa");
                    ConvertionRatio = 0;

                    break;

            }// Switch End
            double ConvFromMeters = LengthInMetres * ConvertionRatio;
            Console.WriteLine("Długość po konwersji:" + ConvFromMeters);
            Console.ReadKey();
            return ConvFromMeters;

        }
    }
}
